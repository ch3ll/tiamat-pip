# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.0] - 2020-10-13
### Added
- Start keeping a changes log
- Allow showing what the library is doing by adding `TIAMAT_PIP_DEBUG=1` to the environment
- `set_user_site_packages_path` now optionaly creates the path. Set to true by default.
- tiamat-pip will now make sure the python headers are included in the generated binary so
that these are available when `pip install`'ing a package than needs to link to the "embedded"
python interpreter.

## [0.10.0] - 2020-10-09
### Added
- Make sure `pypath` comes first in sys.path

## [0.10.0rc1] - 2020-10-08
### Added
- Stopped changing permissions on the `pypath` directory.
- Error out on missing `pypath` directory

## [0.9.0] - 2020-10-01
### Added
- First working version of the project
